package com.kshrd.btbmybatisspringdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BtbMybatisSpringDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(BtbMybatisSpringDemoApplication.class, args);
    }

}
