package com.kshrd.btbmybatisspringdemo.controller;

import com.kshrd.btbmybatisspringdemo.model.Student;
import com.kshrd.btbmybatisspringdemo.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class StudentController {
// injection

    @Autowired
    StudentService studentService;


    @GetMapping()
public  String index(){
//        System.out.println("Here is the value of the students ");
//       studentService.getAllStudentByProvider().stream().forEach(System.out::println);
////

   /*     Student newStudent = new Student(8,"Alexa Updated","Male","I love Web",2);
        //int insertStatus = studentService.insertUser(newStudent);
        int updateStatus = studentService.updateUser(newStudent);

        System.out.println("insertStatus value : "+updateStatus);


        System.out.println(   studentService.findStudentById(8));

        int deleteStatus = studentService.deleteUser(8);
        System.out.println("Here is the delete Status : "+deleteStatus);

*/

        Student student = new Student();

        student.setId(2);
        student.setUsername("Cher Nich");


        System.out.println("Here is the value of the student: ");
        studentService.getStudentByCondition(student).stream().forEach(System.out::println);

        return "index";
    }




}
