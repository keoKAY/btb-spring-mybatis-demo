package com.kshrd.btbmybatisspringdemo.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Student {


//    property
    private int id;
    private String username;
    private String gender;
    private String bio;
    private int course_id;
    private Course course;
    private Role roles;
}
