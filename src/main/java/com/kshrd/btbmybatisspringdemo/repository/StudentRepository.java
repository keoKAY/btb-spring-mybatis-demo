package com.kshrd.btbmybatisspringdemo.repository;

import com.kshrd.btbmybatisspringdemo.model.Course;
import com.kshrd.btbmybatisspringdemo.model.Role;
import com.kshrd.btbmybatisspringdemo.model.Student;
import com.kshrd.btbmybatisspringdemo.repository.provider.StudentProvider;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StudentRepository {




    @SelectProvider(type = StudentProvider.class,method = "getAllUser")
    List<Student>getAllStudentByProvider();


    @SelectProvider(type = StudentProvider.class,method = "getStudentByCondition")
    List<Student> getStudentByCondition(@Param("student") Student student) ;


    @Select("select * from students")
    @Results({
            @Result(property ="id" ,column ="id" ),
            @Result(property = "username",column = "username"),
            @Result(property = "bio",column = "bio"),
            @Result(property = "course_id",column = "course_id"),
            @Result(property = "course",column = "course_id" , one = @One(select = "getCourseByID"))   ,
            @Result(property = "roles",column = "id" ,many = @Many(select = "getRolesByUserId"))

    })
    public List<Student> getAllStudents();
    @Select("select  * from course where id=#{course_id}")
    public Course getCourseByID(int course_id);

    @Select("select roles.id,roles.role_name from roles inner join user_role ur on roles.id = ur.role_id where ur.user_id =#{id}")
    public List<Role> getRolesByUserId(int id);




    // findByID
    @Select("select * from students where id=#{id}")
    public Student findStudentById(int id);

    // insert user
    @Insert("insert into students(id, username,gender, bio,course_id) values (#{student.id},#{student.username},#{student.gender},#{student.bio},#{student.course_id})")
    public int insertStudent(@Param("student") Student student);

//    update
    @Update("update students set username=#{student.username},gender=#{student.gender},bio=#{student.bio},course_id=#{student.course_id} where id=#{student.id}")
    public int updateStudent (@Param("student") Student student);

    // delete
    @Delete("delete from students where id=#{id}")
    public int deleteStudent(int id );




}
