package com.kshrd.btbmybatisspringdemo.repository.provider;

import com.kshrd.btbmybatisspringdemo.model.Student;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.jdbc.SQL;

import java.util.List;

public class StudentProvider {


    // get All students

    public String getAllUser(){

        return new SQL(){{
            SELECT("*");
            FROM("students");


        }}.toString();
    }



    // student : null get all student , username : getbyusername, id = getbyid

    public String getStudentByCondition(@Param("student") Student student){
        return  new SQL(){{
           SELECT("*");
           FROM("students");

           if (student==null){
               WHERE("1=1");
               //
           }else {
               // if username is empty :
               if (student.getUsername().isEmpty()){
                   WHERE("id=#{student.id}");
               }else {
                   WHERE("username like #{student.username}");
               }

           }


        }}.toString();
    }

}
