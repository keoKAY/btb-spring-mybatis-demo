package com.kshrd.btbmybatisspringdemo.service.serviceImp;

import com.kshrd.btbmybatisspringdemo.model.Student;
import com.kshrd.btbmybatisspringdemo.repository.StudentRepository;
import com.kshrd.btbmybatisspringdemo.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentServiceImp implements StudentService {

    // call to repo : inject

    @Autowired
    StudentRepository studentRepository;

    @Override
    public List<Student> getAllStudent() {

        // update your code here.....
//        List<Student> findAll = studentRepository.getAllStudents();
        return studentRepository.getAllStudents();
    }

    @Override
    public Student findStudentById(int id) {
        return studentRepository.findStudentById(id);
    }

    @Override
    public int insertUser(Student student) {
        return studentRepository.insertStudent(student);
    }

    @Override
    public int updateUser(Student student) {
        return studentRepository.updateStudent(student);
    }

    @Override
    public int deleteUser(int id) {
        return studentRepository.deleteStudent(id);
    }

    @Override
    public List<Student> getAllStudentByProvider() {
        return studentRepository.getAllStudentByProvider();
    }

    @Override
    public List<Student> getStudentByCondition(Student student) {
        return studentRepository.getStudentByCondition(student);
    }
}
