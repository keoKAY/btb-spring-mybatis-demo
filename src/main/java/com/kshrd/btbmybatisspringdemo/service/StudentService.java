package com.kshrd.btbmybatisspringdemo.service;

import com.kshrd.btbmybatisspringdemo.model.Student;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface StudentService {


    public List<Student>getAllStudent();
    // find by id
    public Student findStudentById(int id );
    // insert
    public int insertUser(Student student);
    // update
    public int updateUser(Student student);
    //delete
    public int deleteUser(int id);

    List<Student>getAllStudentByProvider();
    List<Student> getStudentByCondition(Student student) ;
}
